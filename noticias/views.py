from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy, reverse
from .models import Post, Comment
from .forms import NoticiaForm, CommentForm
from django.views import generic
from django.http import HttpResponseRedirect

def about(request):
    context = {}
    return render(request, 'noticias/about.html', context)

class NoticiaListView(generic.ListView):
    model = Post
    template_name = 'noticias/index.html'

class NoticiaDetailView(generic.DetailView):
    model = Post
    template_name = 'noticias/detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post = self.get_object()
        comentarios = Comment.objects.filter(noticia=post)
        context['comentarios'] = comentarios
        return context

def busca_noticias(request):
    context = {}
    if request.GET.get('query',False):
        search_term = request.GET['query'].lower()
        noticias_list = Post.objects.filter(titulo__icontains = search_term)
        context = {'noticias_list':noticias_list}
    return render(request,'noticias/busca.html',context)

class NoticiaCreateView(generic.CreateView):
    model = Post
    template_name = 'noticias/create.html'
    form_class = NoticiaForm
    success_url = reverse_lazy('index')

class NoticiaDeleteView(generic.DeleteView):
    model = Post
    template_name = 'noticias/delete.html'
    success_url = reverse_lazy('index')

class NoticiaUpdateView(generic.UpdateView):
    model = Post
    template_name = 'noticias/update.html'
    form_class = NoticiaForm
    success_url = reverse_lazy('index')

def create_review(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            review_author = form.cleaned_data["author"]
            review_text = form.cleaned_data["text"]
            review_data = form.cleaned_data['data']
            review = Comment(author=review_author, text=review_text, noticia=post, data=review_data)
            review.save()
            return HttpResponseRedirect(reverse("detail", 1))
    else:
        form = CommentForm()
        context = {"form": form, "noticia": post}
        return render(request, "noticias/review.html", context)


