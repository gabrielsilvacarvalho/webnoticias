from django.db import models
from django.conf import settings
# Create your models here.

class Post(models.Model):
    titulo = models.CharField(max_length=255)
    conteudo = models.TextField()
    data = models.DateTimeField()
    poster_url = models.URLField(max_length=200, null=True)

    def __str__(self):
        return f'{self.titulo} ({self.data})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=25)
    noticia = models.ForeignKey(Post, on_delete=models.CASCADE)
    data = models.DateTimeField()

    def __str__(self):
        return f'"{self.text}" - {self.author.username} - {self.data}'
