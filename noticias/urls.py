from django.urls import path

from . import views

urlpatterns = [
    path('',views.NoticiaListView.as_view(), name='index'),
    path('/about', views.about, name='about'),
    path('busca/',views.busca_noticias,name='busca'),
    path('create/',views.NoticiaCreateView.as_view(),name='create'),
    path('update/<int:pk>/',views.NoticiaUpdateView.as_view(),name="update"),
    path('delete/<int:pk>/',views.NoticiaDeleteView.as_view(),name="delete"),
    path('<int:post_id>/review/', views.create_review, name='review'),
    path('<int:pk>/',views.NoticiaDetailView.as_view(),name="detail"),
]
