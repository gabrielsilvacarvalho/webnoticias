from django.forms import ModelForm, DateTimeInput
from .models import Comment, Post

class NoticiaForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'titulo',
            'data',
            'poster_url',
            'conteudo',
        ]
        labels = {
            'titulo': 'Título',
            'data': 'Data de Lançamento',
            'poster_url': 'URL do Poster',
            'conteudo': 'Conteúdo',
        }
        widgets = {
            'data': DateTimeInput(attrs={'type': 'datetime-local'}),
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            "author",
            "text",
            "data",
        ]
        labels = {
            "author": "Usuário",
            "text": "Resenha",
            "data": "Escrito em",
        }
        widgets = {
            'data': DateTimeInput(attrs={'type': 'datetime-local'}),
        }


